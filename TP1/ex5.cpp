#include <iostream>
#include <cmath>

using namespace std;

struct vec2 {
  int x;
  int y;
};

bool isMandelbrot(vec2 z1, vec2 p, int n);

int main() {

  vec2 z1;
  z1.x = 3;
  z1.y = 2;

  vec2 p;
  p.x = 1;
  p.y = 5;

  cout << isMandelbrot(z1, p, 6);

  return 0;
}

bool isMandelbrot(vec2 z1, vec2 p, int n) {
  if (n > 0) {
    int z1x2 = z1.x * z1.x;
    int z1y2 = z1.y * z1.y;
    if ((int)sqrt(z1x2 + z1y2) < 2) {
      return true;
    }else {
      vec2 z3;
      z3.x = z1x2 - z1y2 + p.x;
      z3.y = (2 * z1.x * p.y) + p.y;
      return isMandelbrot(z3, p, n - 1);
    }
  }
  return false;
}