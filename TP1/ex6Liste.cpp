#include <iostream>
using namespace std;

struct Noeud {
  int valeur;
  struct Noeud *suivant = nullptr;
};

void afficheListe(Noeud *lst);
void ajoute(Noeud *lst, int nb);
int recupere(Noeud *lst, int nb);
int cherche(Noeud *lst, int n);
void stocke(Noeud *lst, int n, int valeur);

int main() {
  Noeud *lst = new Noeud;
  lst->valeur = -1; // inutile

  ajoute(lst, 6);
  ajoute(lst, 7);
  ajoute(lst, 10);
  ajoute(lst, 12);
  ajoute(lst, 15);

  // cout << cherche(lst, 12) << endl;

  // cout << recupere(lst, 3) << endl;

  // stocke(lst, 1, 18);

  afficheListe(lst);

  delete lst;

  return 0;
}

void ajoute(Noeud *lst, int nb) {
    Noeud *noeud = lst;
    while (noeud->suivant != nullptr) {
      noeud = noeud->suivant;
    }

    Noeud *suiv = new Noeud;
    if(suiv == nullptr) {
      exit(1);
    }
    suiv->valeur = nb;
    noeud->suivant = suiv;
}

int recupere(Noeud *lst, int n) {
  Noeud *noeud = lst->suivant;
  int i = 0;
  while (noeud != nullptr) {
    if(i == n) {
      return noeud->valeur;
    }
    i++;
    noeud = noeud->suivant;
  }

  return -1;
}

int cherche(Noeud *lst, int n) {
  Noeud *noeud = lst->suivant;
  int i = 0;
  while (noeud != nullptr) {
    if(noeud->valeur == n) {
      return i;
    }
    i++;
    noeud = noeud->suivant;
  }

  return -1;
}

void stocke(Noeud *lst, int n, int valeur) {
  Noeud *noeud = lst->suivant;
  int i = 0;
  while (noeud != nullptr) {
    if(i == n) {
      noeud->valeur = valeur;
      break;
    }
    i++;
    noeud = noeud->suivant;
  }
}

void afficheListe(Noeud *lst) {
  Noeud *noeud = lst;
  while (noeud->suivant != nullptr) {
    noeud = noeud->suivant;
    cout << noeud->valeur << endl;
    
  }
}