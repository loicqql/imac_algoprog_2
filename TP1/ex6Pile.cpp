#include <iostream>

using namespace std;

struct Noeud {
  int valeur;
  struct Noeud *suiv;
};

struct Liste {
  struct Noeud *suiv;
};

void afficheListe(Liste *lst);
Noeud * cherche(Liste, int n);
void pousser_file(Liste *lst, int nb);
int retirer_file(Liste *lst);

void pousser_pile(Liste *lst, int nb);
int retirer_pile(Liste *lst);

int main() {
  Liste *lst = new Liste;

  // pousser_file(lst, 6);
  // pousser_file(lst, 7);
  // pousser_file(lst, 2);
  // cout << retirer_file(lst) << endl;
  // afficheListe(lst);

  pousser_pile(lst, 6);
  pousser_pile(lst, 7);
  pousser_pile(lst, 2);
  cout << retirer_pile(lst) << endl;
  afficheListe(lst);
  

  delete lst;

  return 0;
}

// FILE 

void pousser_file(Liste *lst, int nb) {
  Noeud* noeud = lst->suiv;

  if(noeud == nullptr) { // rien dans la lst
    Noeud *suiv = new Noeud;
    if(suiv == nullptr) {
      exit(1);
    }
    suiv->valeur = nb;
    lst->suiv = suiv;
  } else {

    while (noeud->suiv != nullptr) {
      noeud = noeud->suiv;
    }

    Noeud *suiv = new Noeud;
    if(suiv == nullptr) {
      exit(1);
    }
    suiv->valeur = nb;
    noeud->suiv = suiv;
  }  
}

int retirer_file(Liste *lst) {
  int value = lst->suiv->valeur;
  lst->suiv = lst->suiv->suiv;
  return value;
}

// PILE

void pousser_pile(Liste *lst, int nb) {
  Noeud* noeud = lst->suiv;

  if(noeud == nullptr) { // rien dans la lst
    Noeud *suiv = new Noeud;
    if(suiv == nullptr) {
      exit(1);
    }
    suiv->valeur = nb;
    lst->suiv = suiv;
  } else {

    Noeud *prev = new Noeud;
    if(prev == nullptr) {
      exit(1);
    }
    prev->valeur = nb;
    lst->suiv = prev;
    prev->suiv = noeud;
  }
}

int retirer_pile(Liste *lst) {
  int value = lst->suiv->valeur;
  lst->suiv = lst->suiv->suiv;
  return value;
}

void afficheListe(Liste *lst) {
  Noeud* noeud = lst->suiv;
  while (noeud->suiv != nullptr) {
    cout << noeud->valeur;
    noeud = noeud->suiv;
  }
  cout << noeud->valeur;
}