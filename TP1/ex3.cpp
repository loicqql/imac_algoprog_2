#include <iostream>
using namespace std;

int search(int value, int array[], int size);

int main() {
  int da[5] = {2, 3, 4, 5, 6};
  cout << search(6, da, 5);

  return 0;
}

int search(int value, int array[], int size) {

  int index = size - 1;

  if (array[index] == value) {
    return index;
  }

  return search(value, array, index);
}