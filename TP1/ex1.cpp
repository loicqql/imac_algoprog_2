#include <iostream>
using namespace std;

int power(int value, long n);

int main() {
  cout << power(2, 3);

  return 0;
}

int power(int value, long n) {
  if (n == 1) {
    return value;
  }
  return value * power(value, n - 1);
}