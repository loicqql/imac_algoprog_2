#include <iostream>
using namespace std;

void allEvens(int evens[], int array[], int *eventSize, int arraySize);

int main() {
  int eSize = 0;
  int aSize = 5;
  int e[100];
  int a[aSize] = {2, 3, 4, 5, 6};
  allEvens(e, a, &eSize, aSize);

  for (int i = 0; i < eSize; i++) {
    cout << e[i];
  }

  return 0;
}

void allEvens(int *evens, int array[], int *eventSize, int arraySize) {

  int index = arraySize - 1;

  if ((array[index] % 2) == 0) {
    evens[*eventSize] = array[index];
    (*eventSize)++;
  }

  if (arraySize > 1) {
    return allEvens(evens, array, eventSize, index);
  }
}