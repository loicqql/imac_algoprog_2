#include <iostream>

using namespace std;

struct DynaTableau {
  int* data;
  int size;
  int capacity;
};

void initialise(DynaTableau* dt, int capacite);
int cherche(DynaTableau* dt, int nb);
int recupere(DynaTableau* dt, int index);
void ajoute(DynaTableau* dt, int nb);
void stocke(DynaTableau* dt, int index, int valeur);
void afficheDt(DynaTableau* dt);

int main() {
  DynaTableau *dt = new DynaTableau;

  initialise(dt, 2);
  ajoute(dt, 6);
  ajoute(dt, 7);
  ajoute(dt, 8);

  // cout << recupere(dt, 2) << endl;
  // cout << cherche(dt, 8) << endl;
  // cout << cherche(dt, 18) << endl;
  // stocke(dt, 1, 18);

  afficheDt(dt);

  delete dt;

  return 0;
}

void initialise(DynaTableau* dt, int capacite) {
  dt->data = new int[capacite];
  dt->size = 0;
  dt->capacity = capacite;
}

void ajoute(DynaTableau* dt, int nb) {
  if(dt->size + 1 > dt->capacity) {
    dt->capacity = dt->capacity + 2;
    int* newTab = new int[dt->capacity];
    if(newTab == nullptr) {
      exit(1);
    }

    for (int i = 0; i < dt->size; i++) {
      newTab[i] = dt->data[i];
    }

    delete[] dt->data;
    dt->data = newTab;
  }

  dt->data[dt->size] = nb;
  dt->size = dt->size + 1;
}

int recupere(DynaTableau* dt, int index) {
  if(index  >= dt->size) {
    exit(1);
  }
  return dt->data[index];
}

int cherche(DynaTableau* dt, int nb) {
  for (int i = 0; i < dt->size; i++) {
    if(dt->data[i] == nb) {
      return i;
    }
  }
  
  return -1;
}

void stocke(DynaTableau* dt, int index, int valeur) {
  if(index  >= dt->size) {
    exit(1);
  }
  dt->data[index] = valeur;
}


void afficheDt(DynaTableau* dt) {
  for (int i = 0; i < dt->size; i++) {
    cout << dt->data[i] << endl;
  }
  
}