#include <iostream>
using namespace std;

int TAILLE = 5;

void sort(int tab[], int taille);
void afficheTab(int tab[], int taille);
void insert(int tabToInsert[], int * tailleTab, int index, int valueToInsert);

int main() {
    

    // int tab[TAILLE] = {3, 9, 5, 6, 3};
    int tab[TAILLE] = {5, 9, 4, 6, 6};


    sort(tab, TAILLE);

    afficheTab(tab, TAILLE);
    
    return 0;
}

void sort(int tab[], int taille) {
    int result[taille];
    result[0] = tab[0];
    int tailleResult = 1;

    int zMoins;

    for (int i = 1; i < taille; i++) {
        zMoins = tailleResult;
        for (int j = tailleResult; j > 0; j--) {
            if(tab[i] < result[j]) {
                zMoins = j;
            }
        }
        insert(result, &tailleResult, zMoins, tab[i]);
    }

    for (int i = 0; i < taille; i++) {
        tab[i] = result[i];
    }
    
}

void insert(int tabToInsert[], int * tailleTab, int index, int valueToInsert) {

    for (int i = *tailleTab; i > index; i--) {
        tabToInsert[i] = tabToInsert[i - 1];
    }

    tabToInsert[index] = valueToInsert;

    (*tailleTab)++;

}

void afficheTab(int tab[], int taille) {
    cout << endl;
    for (int i = 0; i < taille; i++) {
        cout << tab[i];
    }
    cout << endl;
    
}