#include <iostream>
using namespace std;

const int TAILLE = 6;

int * sort(int tab[], int taille);
void afficheTab(int tab[], int taille);

int main() {
    

    int tab[TAILLE] = {4, 4, 1, 3, 5, 9};

    sort(tab, TAILLE);

    afficheTab(tab, TAILLE);
    
    return 0;
}

int * sort(int tab[], int taille) {
    if(taille <= 1) {
        return tab;
    }
        
    int t = tab[(int)(taille / 2)];
    int lowers[taille];
    int lowersTaille = 0;
    int greaters[taille];
    int greatersTaille = 0;
    for (int i = 0; i < taille; i++) {
        if(tab[i] < t) {
            lowers[lowersTaille] = tab[i];
            lowersTaille++;
        }
        if(tab[i] > t) {
            greaters[greatersTaille] = tab[i];
            greatersTaille++;
        }           
    }

    sort(lowers, lowersTaille);
    sort(greaters, greatersTaille);

    int z = 0;
    for (int j = 0; j < lowersTaille; j++) {
        tab[z] = lowers[j];
        z++;
    }
    tab[z] = t;
    z++;
    for (int h = 0; h < greatersTaille; h++) {
        tab[z] = greaters[h];
        z++;
    }

    return tab;
}

void afficheTab(int tab[], int taille) {
    cout << endl;
    for (int i = 0; i < taille; i++) {
        cout << tab[i];
    }
    cout << endl;
    
}