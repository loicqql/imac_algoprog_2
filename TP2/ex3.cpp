#include <iostream>
using namespace std;

int TAILLE = 6;

void sort(int tab[], int taille);
void afficheTab(int tab[], int taille);
void insert(int tabToInsert[], int * tailleTab, int index, int valueToInsert);

int main() {
    

    int tab[TAILLE] = {4, 6, 1, 3, 5, 10};

    sort(tab, TAILLE);

    afficheTab(tab, TAILLE);
    
    return 0;
}

void sort(int tab[], int taille) {
    for (int i = 0; i < taille; i++) {
        if(i != taille - 1) {
            if(tab[i] > tab[i+1]) {
                int a = tab[i];
                tab[i] = tab[i+1];
                tab[i+1] = a;
            }
        }
    }
}

void afficheTab(int tab[], int taille) {
    cout << endl;
    for (int i = 0; i < taille; i++) {
        cout << tab[i];
    }
    cout << endl;
    
}