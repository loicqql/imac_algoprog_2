#include <iostream>
using namespace std;

const int TAILLE = 5;

void sort(int tab[], int taille);
void afficheTab(int tab[], int taille);

int main() {
    

    // int tab[TAILLE] = {8, 9, 5, 6, 7};
    int tab[TAILLE] = {3, 9, 5, 6, 7};

    sort(tab, TAILLE);

    afficheTab(tab, TAILLE);
    
    return 0;
}

void sort(int tab[], int taille) {
    int zMin;
    int min;
    for (int j = 0; j < taille; j++) {
        zMin = j;
        min = tab[j];
        for (int i = j; i < taille; i++) {
            if(tab[i] < min) {
                min = tab[i];
                zMin = i;
            }
        }
        int a = tab[j];
        tab[j] = min;
        tab[zMin] = a;
    }
}

void afficheTab(int tab[], int taille) {
    cout << endl;
    for (int i = 0; i < taille; i++) {
        cout << tab[i];
    }
    cout << endl;
    
}