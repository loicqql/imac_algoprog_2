#include <iostream>
using namespace std;

const int TAILLE = 6;

int * sort(int tab[], int taille);
int minTab(int tab[], int taille);
void afficheTab(int tab[], int taille);

int main() {

    int tab[TAILLE] = {4, 2, 7, 3, 5, 9};

    sort(tab, TAILLE);

    afficheTab(tab, TAILLE);
    
    return 0;
}

int * sort(int tab[], int taille) {
    if(taille <= 1) {
        return tab;
    }
        
    int t = (int)(taille / 2);
    int t1[taille];
    int t1Taille = 0;
    int t2[taille];
    int t2Taille = 0;
    for (int i = 0; i < t; i++) {
        t1[t1Taille] = tab[i];
        t1Taille++;
    }
    for (int i = t; i < taille; i++) {
        t2[t2Taille] = tab[i];
        t2Taille++;
    }

    sort(t1, t1Taille);
    sort(t2, t2Taille);

    int i = 0;
    int a, b;
    while (i < taille) {
        if(t1Taille > 0) { // dans le cas on il reste 1 valeur dans t2
            a = minTab(t1, t1Taille);
            b = minTab(t2, t2Taille);
            tab[i] = a <= b ? a : b;
            tab[i + 1] = b <= a ? a : b;    
            t1Taille--;
            t2Taille--;
            i = i + 2;
        }else {
            tab[i] = minTab(t2, t2Taille);
            i++;
        }
    }

    return tab;
}

int minTab(int tab[], int taille) {
    if(taille < 1) {
        exit(0);
    }

    if(taille == 1) {
        return tab[0];
    }

    int z = 0;
    int a = tab[z];
    
    for (int i = 0; i < taille; i++) {
        if(tab[i] < a) {
            a = tab[i];
            z = i;
        }
    }

    tab[z] = tab[taille - 1];

    return a;

}

void afficheTab(int tab[], int taille) {
    cout << endl;
    for (int i = 0; i < taille; i++) {
        cout << tab[i];
    }
    cout << endl;
    
}