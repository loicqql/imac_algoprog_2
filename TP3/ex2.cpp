#include <iostream>
using namespace std;

void binarySearchAll(int tab[], int taille, int toSearch, int * indexMin, int * indexMax);
void binarySearchAll2(int tab[], int taille, int toSearch, int * indexMin, int * indexMax);

int main() {

    int a, b;
    int taille = 6;

    int tab[taille] = {2, 3, 7, 7, 7, 8};

    binarySearchAll(tab, taille, 7, &a, &b);

    cout << a << endl;
    cout << b << endl;
    
    return 0;
}

void binarySearchAll(int tab[], int taille, int toSearch, int * indexMin, int * indexMax) {
    int start = 0;
    int end = taille;
    int mid;

    int foundIndex = - 1;

    while (start < end) {
        mid = (start + end) / 2;
        if(toSearch > tab[mid]) {
            start = mid + 1;
        }else if(toSearch < tab[mid]) {
            end = mid;
        }else {
            *indexMin = end = mid;
        }
    }

    start = 0;
    end = taille;

    while (start < end) {
        mid = (start + end) / 2;
        if(toSearch > tab[mid]) {
            start = mid + 1;
        }else if(toSearch < tab[mid]) {
            end = mid;
        }else {
            *indexMax = mid;
            start = mid +1;
        }
    }
}

// une autre façon de faire - moins performant

void binarySearchAll2(int tab[], int taille, int toSearch, int * indexMin, int * indexMax) {
    int start = 0;
    int end = taille;
    int mid;

    *indexMin = *indexMax = -1;
    int foundIndex = -1;

    while (start < end) {
        mid = (start + end) / 2;
        if(toSearch > tab[mid]) {
            start = mid + 1;
        }else if(toSearch < tab[mid]) {
            end = mid;
        }else {
            foundIndex = mid;
            *indexMin = *indexMax = mid;
            int a = true;
            int b = true;
            while(a && b) {
                if(*indexMin >= 1) {
                    if(tab[*indexMin - 1] == tab[foundIndex]) {
                        *indexMin = *indexMin - 1;
                    }else {
                        a = false;
                    }
                }else {
                    a = false;
                }
                if(*indexMax < taille) {
                    if(tab[*indexMax + 1] == tab[foundIndex]) {
                        *indexMax = *indexMax + 1;
                    }else {
                        b = false;
                    }
                }else {
                    b = false;
                }
            }
            break;
        }
    }
}