#include <iostream>
using namespace std;

struct SearchTreeNode;
SearchTreeNode* createNode(int value);
void afficheLeaves(SearchTreeNode* leaves[], uint leavesCount);

struct SearchTreeNode {
    SearchTreeNode* left;
    SearchTreeNode* right;
    int value;

    void initNode(int value) {
        this->value = value;
        this->left = nullptr;
        this->right = nullptr;
    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child
        if(this->value >= value) {
            if(this->left == nullptr) {
                this->left = createNode(value);
            }else {
                this->left->insertNumber(value);   
            }
        }else {
            if(this->right == nullptr) {
                this->right = createNode(value);
            }else {
                this->right->insertNumber(value);   
            }
        }
    }

	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        if(this->isLeaf()) {
            return 1;
        }
        int a = 0;
        int b = 0;
        if(this->left != nullptr) {a = this->left->height();}
        if(this->right != nullptr) {b = this->right->height();}
        if(a > b) {
            return a + 1;
        }else {
            return b + 1;
        }
    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        if(this->isLeaf()) {
            return 1;
        }
        int a = 0;
        int b = 0;
        if(this->left != nullptr) {a = this->left->nodesCount();}
        if(this->right != nullptr) {b = this->right->nodesCount();}

        return a + b + 1;
	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        return left == nullptr && right == nullptr ? true : false;
	}

	void allLeaves(SearchTreeNode* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
        if(this->isLeaf()) {
            leaves[leavesCount] = this;
            leavesCount++;
        }else {
            if(this->left != nullptr) {this->left->allLeaves(leaves, leavesCount);}
            if(this->right != nullptr) {this->right->allLeaves(leaves, leavesCount);}
        }
	}

	void inorderTravel(SearchTreeNode* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        if(this->left != nullptr) {this->left->inorderTravel(nodes, nodesCount);}
        nodes[nodesCount] = this;
        nodesCount++;
        if(this->right != nullptr) {this->right->inorderTravel(nodes, nodesCount);}
        
	}

	void preorderTravel(SearchTreeNode* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
        nodes[nodesCount] = this;
        nodesCount++;
        if(this->left != nullptr) {this->left->preorderTravel(nodes, nodesCount);}
        if(this->right != nullptr) {this->right->preorderTravel(nodes, nodesCount);}
	}

	void postorderTravel(SearchTreeNode* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
        if(this->left != nullptr) {this->left->postorderTravel(nodes, nodesCount);}
        if(this->right != nullptr) {this->right->postorderTravel(nodes, nodesCount);}
        nodes[nodesCount] = this;
        nodesCount++;
	}

	SearchTreeNode* find(int value) {
        // find the node containing value
        if(this->value == value) {
            return this;
        }else {
            if(this->left != nullptr && value < this->value) {return this->left->find(value);}
            if(this->right != nullptr && value > this->value) {return this->right->find(value);}
            return nullptr;
        }
        
	}

    void reset() {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    SearchTreeNode(int value) {initNode(value);}
    ~SearchTreeNode() {}
};

SearchTreeNode* createNode(int value) {
    return new SearchTreeNode(value);
}

int main() {

    SearchTreeNode* node;

    node = createNode(9);

    node->insertNumber(20);
    node->insertNumber(11);
    node->insertNumber(10);
    node->insertNumber(13);
    node->insertNumber(6);
    node->insertNumber(1);
    node->insertNumber(7);
    node->insertNumber(18);
    //Testé avec le debugger de vscode

    cout << node->height() << endl; // 5 
    cout << node->nodesCount() << endl; // 9

    //allLeaves
    SearchTreeNode* leaves[10];
    uint leavesCount = 0;
    node->allLeaves(leaves, leavesCount);
    afficheLeaves(leaves, leavesCount);
    cout << leavesCount << endl; // 4


    //Travels
    SearchTreeNode* nodes[20];
    for (int i = 0; i < 20; i++) { // clear le tableau
        nodes[i] = nullptr;
    }
    uint nodesCount = 0;
    // node->inorderTravel(nodes, nodesCount);
    // node->preorderTravel(nodes, nodesCount);
    node->postorderTravel(nodes, nodesCount);
    afficheLeaves(nodes, nodesCount);
    cout << nodesCount << endl;

    //Find
    SearchTreeNode* nodeF = node->find(10);
    if(nodeF == nullptr) {
        cout << "non";
    }else {
        cout << "oui";
    }

    return 0;
}

void afficheLeaves(SearchTreeNode* leaves[], uint leavesCount) {
    for (int i = 0; i < leavesCount; i++) {
        cout << leaves[i]->value;
    }
    cout << endl;
}