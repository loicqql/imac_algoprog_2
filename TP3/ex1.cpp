#include <iostream>
using namespace std;

int binarySearch(int tab[], int taille, int toSearch);

int main() {

    int taille = 6;

    int tab[taille] = {1, 2, 4, 7, 8, 9};

    cout << binarySearch(tab, taille, 4) << endl;
    cout << binarySearch(tab, taille, 5) << endl;

    // afficheTab(tab, taille);
    
    return 0;
}

int binarySearch(int tab[], int taille, int toSearch) {
    int start = 0;
    int end = taille;
    int mid;

    int foundIndex = - 1;

    while (start < end) {
        mid = (start + end) / 2;
        if(toSearch > tab[mid]) {
            start = mid + 1;
        }else if(toSearch < tab[mid]) {
            end = mid;
        }else {
            foundIndex = mid;
            break;
        }
    }

    return foundIndex;
}

void afficheTab(int tab[], int taille) {
    cout << endl;
    for (int i = 0; i < taille; i++) {
        cout << tab[i];
    }
    cout << endl;
}