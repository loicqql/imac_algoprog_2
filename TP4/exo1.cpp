#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex) {
    return 2*nodeIndex+1;
}

int Heap::rightChild(int nodeIndex) {
    return 2*nodeIndex+2;
}

int parent(int nodeIndex) {
    return (nodeIndex-1)/ 2;
}

void Heap::insertHeapNode(int heapSize, int value) {
	// use (*this)[i] or this->get(i) to get a value at index i

	int i = heapSize;
    (*this)[i] = value;
    while (i > 0 && this->get(i) > this->get(parent(i))) {
        this->swap(i, parent(i));
        i = parent(i);
    }
}

void Heap::heapify(int heapSize, int nodeIndex) {
	// use (*this)[i] or this->get(i) to get a value at index i

    int indexm = nodeIndex;

    indexm = this->leftChild(nodeIndex) < heapSize && this->get(indexm) < this->get(this->leftChild(nodeIndex)) ? this->leftChild(nodeIndex) : indexm; // sinon rien

    indexm = this->rightChild(nodeIndex) < heapSize && this->get(indexm) < this->get(this->rightChild(nodeIndex)) ? this->rightChild(nodeIndex) : indexm; // sinon rien

    if(indexm != nodeIndex){
        this->swap(nodeIndex, indexm);
        this->heapify(heapSize, indexm);
    }

}

void Heap::buildHeap(Array& numbers) {
    for(int value: numbers) {
        this->insertHeapNode(numbers.size(), value);
    }
}

void Heap::heapSort() {
    for (int i = (this->size()-1); i > 0; i--) {
        this->swap(0, i);
        this->heapify(i, 0);
    }
}

int main(int argc, char *argv[]) {
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
